Please create an account for me / my team on the following services:

(delete all that don't apply)

 - pixelfed.debian.social
 - peertube.debian.social
 - pleroma.debian.social
 - wordpress.debian.social
 - writefreely.debian.social

 I understand that these services are in a beta state and may still have some
 issues.

 I agree to uphold Debian's community standards, including the Debian code
 of Conduct, when using these services.
 
 If you're not a Debian Developer, please provide your email address as well:
 
 Email: 

If you're applying to creat team account, specify your team name below. "team"
will be appended to any team name:

 Team: 

/label account
/cc @jcc @paddatrapper @rhonda
/confidential
